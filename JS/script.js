/*1. Екранування символів використовується для того щоб вони розпізнавалися як текст, а не як частина коду.*/
/*2. Function declaration - функція визначена як окрема інструкція в основному потоці коду, Function Expression - створення функції всередині будь-якого виразу.*/
/*3. Підйом — це типова поведінка JavaScript, яка переміщує оголошення вгору. Як це працює:
Якщо ми створимо якусь Function declaration та спробуємо викликати її вижче то ніяких помилок не буде.
Якщо ми створимо якусь Function Expression за допомогою var та спробуємо викликати її вижче то отримаємо undefined.
Якщо ми створимо якусь змінну за допомогою var і призначемо їй якесь значення і наприклад зробимо console.log(цю змінну) вище то отримаємо undefined.
Чому так? Бо виконується підйом "оголошення"  до гори коду, тобто наприкладі змінної - огошення піднялось у гору але без самого значення,тому змінна є але значення undefined.
Різниця між var / function та let / const. Перщі ініціалізуються з знченням undefined , другі залишаються не ініціалізованими.
*/

function createNewUser() {
     let firstName = prompt("Вкажіть своє ім'я");
     let lastName = prompt("Вкажіть своє прізвище");
     let birthday = prompt("Вкажіть свою дату народження", "dd.mm.yyyy");
     let arrBirthday = birthday.split('.');
     let userBirthday = new Date();
     userBirthday.setDate(+arrBirthday[0]);
     userBirthday.setMonth(+arrBirthday[1]-1);
     userBirthday.setFullYear(+arrBirthday[2]);
console.log(userBirthday);
     let newUser = {
          nameUser: firstName,
          surnameUser: lastName,
          birthdayUser: birthday,
          getAge() {
               let now = Date.now();
               let age = (now - Date.parse(userBirthday))/31536000000;
               return Math.trunc(age);
          },
          getLogin() {
               let login = (this.nameUser.slice(0, 1)).toUpperCase() + (this.surnameUser).toLowerCase() + userBirthday.getFullYear();
               return login;
          },
     }

     return newUser;
}

let user1 = createNewUser();
console.log(user1);
console.log(user1.getAge());
console.log(user1.getLogin());